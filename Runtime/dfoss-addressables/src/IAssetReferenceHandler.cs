namespace Addressable.Utils
{
	public interface IAssetReferenceHandler
	{
		void Release();
	}
}
