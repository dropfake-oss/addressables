//#define LOG_LOAD_REQUESTS

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Container.Utils;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UIElements;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

namespace Addressable.Utils
{
	public class AddressableManager
	{
		private static AddressableManager? _instance;
		private AsyncOperationHandle<IResourceLocator> _initializeHandle;

		// Separate reference counting when cloning visual tree assets as Addressables does not track this use case
		private readonly Dictionary<string, AsyncOperationHandle<VisualTreeAsset>> _visualTreeAssetCache = new();
		private readonly Dictionary<string, int> _visualTreeAssetInstanceCount = new();

		public static AddressableManager Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new AddressableManager
					{
						_initializeHandle = Addressables.InitializeAsync()
					};
				}

				return _instance;
			}
		}

		public bool Initialized => _initializeHandle.IsDone;

		public void DownloadAssetAsync(string key, Action? onCompleted = null)
		{
			var handle = Addressables.DownloadDependenciesAsync(key, true);
			if (onCompleted != null)
			{
				handle.Completed += _ => onCompleted();
			}
		}

		public void DownloadAssetsAsync(IEnumerable<string> keys, Action? onCompleted = null)
		{
			var handle = Addressables.DownloadDependenciesAsync(keys, Addressables.MergeMode.Union, true);
			if (onCompleted != null)
			{
				handle.Completed += _ => onCompleted();
			}
		}

		public void DownloadLabeledAssetsAsync(string label, Action? onCompleted = null)
		{
			var handle = Addressables.LoadResourceLocationsAsync(label);
			handle.Completed += completionHandle =>
			{
				if (completionHandle.Status == AsyncOperationStatus.Succeeded)
				{
					Addressables.DownloadDependenciesAsync(completionHandle.Result, true);
				}

				Addressables.Release(completionHandle);
			};
		}

		public void DownloadLabeledAssetsAsync(IEnumerable<string> labels, Addressables.MergeMode mode, Action? onCompleted = null)
		{
			var handle = Addressables.LoadResourceLocationsAsync(labels, mode);
			handle.Completed += completionHandle =>
			{
				if (completionHandle.Status == AsyncOperationStatus.Succeeded)
				{
					Addressables.DownloadDependenciesAsync(completionHandle.Result, true);
				}

				Addressables.Release(completionHandle);
			};
		}

		public async Task<GameObject?> InstantiateManagedGameObject(string key)
		{
			var handle = Addressables.InstantiateAsync(key);
			await handle.Task;

			var root = handle.Result;
			if (root != null)
			{
				var wrapper = root.AddComponent<GameObjectOperationHandleWrapper>();
				wrapper.Initialize(handle);
			}

			return root;
		}

		public VisualElement? InstantiateVisualTreeAsset(string key)
		{
			if (!_visualTreeAssetCache.ContainsKey(key))
			{
				var handle = LoadAssetAsync<VisualTreeAsset>(key);
				handle.WaitForCompletion();
				_visualTreeAssetCache.Add(key, handle);
				_visualTreeAssetInstanceCount[key] = 0;
			}

			_visualTreeAssetInstanceCount[key] += 1;
			var cachedHandle = _visualTreeAssetCache[key];
			var asset = cachedHandle.Result;
			var instance = asset != null ? asset.CloneTree() : null;
			return instance;
		}

		public Task<VisualElement?> InstantiateVisualTreeAssetAsync(string key)
		{
			if (!_visualTreeAssetCache.ContainsKey(key))
			{
				var handle = LoadAssetAsync<VisualTreeAsset>(key);
				_visualTreeAssetCache.Add(key, handle);
				_visualTreeAssetInstanceCount[key] = 0;
			}

			_visualTreeAssetInstanceCount[key] += 1;
			var cachedHandle = _visualTreeAssetCache[key];
			var cloningTask = cachedHandle.Task.ContinueWith<VisualElement?>(_ =>
			{
				var asset = cachedHandle.Result;
				var instance = asset != null ? asset.CloneTree() : null;
				return instance;
			});
			return cloningTask;
		}

		[Obsolete("Use the alternative version of this function that takes in a disposable collection to avoid memory leaks")]
		public VisualTreeAsset? LoadAddressableVisualTreeAssetSync(string key)
		{
			var handle = LoadAssetAsync<VisualTreeAsset>(key);
			var asset = handle.WaitForCompletion();
			if (asset != null)
			{
				return asset;
			}

			Debug.LogError($"Failed to load addressable visual tree asset with key: {key}");
			return null;
		}

		public VisualTreeAsset? LoadAddressableVisualTreeAssetSync(string key, DisposableCollection<IDisposable> disposableCollection)
		{
			var handle = LoadAssetAsync<VisualTreeAsset>(key);
			var asset = handle.WaitForCompletion();
			if (asset != null)
			{
				disposableCollection.Add(new DisposableOperationHandleWrapper<VisualTreeAsset>(handle));
				return asset;
			}

			Debug.LogError($"Failed to load addressable visual tree asset with key: {key}");
			return null;
		}

		public AsyncOperationHandle<T> LoadAssetAsync<T>(string key, Action<T?>? onComplete = null) where T : Object
		{
			LogLoadRequest(key);

			var handle = Addressables.LoadAssetAsync<T>(key);
			handle.Completed += completionHandle =>
			{
				if (completionHandle.Status != AsyncOperationStatus.Succeeded)
				{
					Debug.LogError($"Failed to load asset async with key ({key})! Exception: {completionHandle.OperationException}");
				}

				onComplete?.Invoke(completionHandle.Result);
			};

			return handle;
		}

		public DisposableOperationHandleWrapper<T> LoadManagedAssetAsync<T>(string key, Action<T?>? onComplete = null) where T : Object
		{
			var handle = LoadAssetAsync(key, onComplete);
			return new DisposableOperationHandleWrapper<T>(handle);
		}

		public void ReleaseVisualTreeAssetInstance(string key)
		{
			if (!_visualTreeAssetCache.ContainsKey(key))
			{
				Debug.LogError($"Attempting to releasing visual tree asset instance <{key}> with a reference count of 0");
				return;
			}

			_visualTreeAssetInstanceCount[key] -= 1;
			if (_visualTreeAssetInstanceCount[key] <= 0)
			{
				var handle = _visualTreeAssetCache[key];
				_visualTreeAssetCache.Remove(key);
				_visualTreeAssetInstanceCount.Remove(key);
				Addressables.Release(handle);
			}
		}

		public void WaitForCompletionAndRelease<T>(AsyncOperationHandle<T> handle)
		{
			if (!handle.IsValid())
			{
				return;
			}

			if (!handle.IsDone)
			{
				handle.WaitForCompletion();
			}

			Addressables.Release(handle);
		}

		[Conditional("LOG_LOAD_REQUESTS")]
		private static void LogLoadRequest(string key)
		{
			Debug.LogFormat("[Addressables] Loading '{0}'", key);
		}
	}
}
