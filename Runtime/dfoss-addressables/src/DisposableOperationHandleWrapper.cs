using System;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Addressable.Utils
{
	public class DisposableOperationHandleWrapper<T> : IDisposable, IAssetReferenceHandler
	{
		private readonly AsyncOperationHandle<T> _operationHandle;
		private bool _released;

		public DisposableOperationHandleWrapper(AsyncOperationHandle<T> operationHandle)
		{
			_operationHandle = operationHandle;
		}

		~DisposableOperationHandleWrapper()
		{
			Release();
		}

		public void Dispose()
		{
			Release();
		}

		public void Release()
		{
			// TODO: <JI> We should do an early out if the game isn't running anymore to avoid an invalid release error
			// I tried using Application.isPlaying but it doesn't work because it can't be called outside the main thread
			if (_released)
			{
				return;
			}

			_released = true;
			Addressables.Release(_operationHandle);
		}
	}
}
