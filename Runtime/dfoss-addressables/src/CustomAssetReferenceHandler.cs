using System;
using System.Collections;
using System.Collections.Generic;
using Addressable.Utils;
using UnityEngine;

public class CustomAssetReferenceHandler : IAssetReferenceHandler
{
	private bool _released = false;
	private readonly Action _releaseAction;

	public CustomAssetReferenceHandler(Action releaseAction)
	{
		_releaseAction = releaseAction;
	}

	public void Release()
	{
		if (_released)
		{
			return;
		}

		_releaseAction?.Invoke();
		_released = true;
	}
}
