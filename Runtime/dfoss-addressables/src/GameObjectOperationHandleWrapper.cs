using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Addressable.Utils
{
	public class GameObjectOperationHandleWrapper : MonoBehaviour, IAssetReferenceHandler
	{
		private AsyncOperationHandle _operationHandle;
		private bool _released = true;

		public void Initialize(AsyncOperationHandle operationHandle)
		{
			_operationHandle = operationHandle;
			_released = false;
		}

		public void Release()
		{
			if (_released || !Application.isPlaying)
			{
				return;
			}

			_released = true;
			Addressables.ReleaseInstance(_operationHandle);
		}

		private void OnDestroy()
		{
			Release();
		}
	}
}
